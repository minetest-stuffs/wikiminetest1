---
title: start
description: start wiki page
published: 1
date: 2024-03-01T21:16:24.456Z
tags: index, home
editor: markdown
dateCreated: 2024-03-01T19:04:14.495Z
---

# MINETEST WIKI

<img src="assets/grasstitle.jpg" width="100%" height="20">

| [Minetest.org](https://minetest.org)            | [Mods & git](https:/git.minetest.io)         | [Minetest.io](https://minetest.io)          |
| ----------------------------------------------- | -------------------------------------------- | ------------------------------------------- |
| [Source code](https://downloads.minetest.org/)  | [Game Chats](https://irc.minetest.org:9000/) | [Downloads](https://minetest.io/downloads/) |
| [Minetest English wiki](en/)                    | [Minetest Spanish wiki](es/)                 | [Developers submodule](devel)               |
| [How to contribute to wiki](en/contribution.md) | [Como contribuir y ayudar](es/contribution.md) | [Developers submodule](devel)             |

[![Minetest.org](https://img.shields.io/badge/WebPage-minetest-blue)](http://minetest.org)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-orange.svg)](https://creativecommons.org/licenses/by-sa/4.0/)


The Developers' Center is largely for Final Minetest core developers and website admins. However, all visitors are welcome to read content.

![pumpkin_190127.jpg](/assets/pumpkin_190127.jpg)

## About this website

<img src="assets/grasstitle.jpg" width="100%" height="20">

This is a Wiki website.

