---
title: contribution
description: How to help!? how to contribute to minetest and this wiki!
published: 1
date: 2024-03-01T21:16:24.456Z
tags: english, help
editor: markdown
dateCreated: 2024-03-01T19:04:14.495Z
---



The Minetest Wiki is a website aimed to store information about the game and how to play like blocks, items, entities, etc.


New to wiki editing?
--------------------

Don't be afraid to edit it!! like yourself **is easy! Unless trolltest wiki we have a full gui web site! Just click the "Edit"**
tab at the top of pages to edit their contents. **Dont have an account? dont worry read next guidelines:**


How do I start?
---------------

You can create an account or log in before you're able to edit pages without your IP address being displayed in the edit history.
Just login in http://git.minetest.org and wait for approval! The same account and a temporal password will be mailed to you!

After click the edit button, you will have a double panel with preview at the right side (single one in phones with preview at second tab).

Then when you finish to edit just save it and see the new contribution you made it!

Policy and guidelines
---------------------

The wiki can be edited by anyone and is governed by the consensus of its users, 
derived by argument-supported, civil collaboration between them but:

1. Anything that is against the [Minetest LICENSE and/or usage guidelines](licenseusage.md) is not allowed on the wiki, no matter what.
2. Do not vandalize, spam, or otherwise edit disruptively.
3. Keep your behavior civil and assume good faith. Do not harass, stalk, or insult anyone. Address the action, not the person behind it.
4. Avoid misleading readers and other editors. Make sure your contributions to articles are verifiable. Do not add speculation, parody content, or hoaxes to articles.
5. Do not advertise any fan communities or servers. Articles should not contain information about or links to fan communities except in cited references.
6. Do not copy content from other sources, including those that discuss Minetest.org. If you believe there is a legitimate need to use some copyrighted material as a fair use file, mark it as such.
    * If such material is hosted at git.minetest.io or at our repositories it can be used inside wiki and refered!
7. Do not link or embed external videos in articles, unless there is no other way to demonstrate the phenomenon, or the video is official. Do not promote corporate sites please!
8. The wiki is not a file-hosting service. If an uploaded file is not integrated into a useful page soon after the upload, it may be deleted without notice. Uploading images for your userspace is allowed with same policy.
9. Accounts with offensive, misleading, or otherwise inappropriate usernames may be blocked indefinitely.
    * Abuse of multiple accounts for block evasion or other circumvention of policy may result in these accounts being indefinitely blocked.
    * An editor may have more than one account only if all accounts used by the editor are clearly marked.
    * Conversely, each account should only be used by one person. If a group or organization wishes to edit on-wiki, each individual member that will be editing needs to create their own account.

Copyright and content
---------------------

All the users can claim copyright if their own first content but not of the overall content, 
by example, if a wiki page has 30 edits commits and user only put the firs one and the last one, 
such user only can reclaim and use the first editing and related content, the page must be 
stalled and content must be refered to the respective topic, then rest of collaborators must 
create a new page, sustitute the related one and in new page link the first one (moved).
The new created page must not has the same redaction and sections if the old user reclaim 
copyright of old content and is the main structure of.

[![Minetest.org](https://img.shields.io/badge/WebPage-minetest-blue)](http://minetest.org)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-orange.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

