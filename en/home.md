---
title: minetest home
description: main home of the minetest wiki
published: 1
date: 2024-03-01T21:16:24.456Z
tags: home, index, minetest, english
editor: markdown
dateCreated: 2024-03-01T19:04:14.495Z
---

<strong>Welcome to the Minetest wiki.</strong> If you'd like to edit here, check [how to contribute page](contribute.md)

Minetest is a free [gameengine.](https://en.wikipedia.org/wiki/Game_engine) referred to as [voxel.](https://en.wikipedia.org/wiki/Voxel)

1. Think of Minetest as a **Manager of collections of programs. Each program can create or visit blocky game worlds**
2. The blocky **game worlds have themes such as Old West, post apocalypse, ordinary cities, and many more**.
3. **Gameplay vary depending of those game worlds** rules! It can be downloaded or player online!

![images.jpeg](/assets/images.jpeg){width="400"}

Shut up! How to play!?
======================

1. Download the [many options of manager engines](minetest-downloads.md) program and run!
2. Browse the many [gameplays worlds](minetest-games.md) and later launch!
3. Or browse the [network of minetest servers](minetest-servers.md) to play with friends!

------------------------------------------------------------------------

The basics is that **Final Minetest** includes server (who runs the world game) and client (who connect to the server to play the worlds) programs of different types. Its primary server program, **finetestserver,** works with all major Minetest clients dated through mid-2023.

> Note: Post-2023 Trolltest and MultiCraft clients only supported with lasted versions

This website focuses on Final Minetest but it includes some Trolltest documentation as it's partly compatible.

Pages for users
---------------

| Wiki link                               | Observations                                         |
| --------------------------------------- | ---------------------------------------------------- |
| [Play on your browser](play-browser.md) | You can test easyle in the web broser, but with several limitations |
| [PLay on linux](minetest-packages.md)   | We take care of your linux, check this if you want a package for installation  |
| [Windows minetest](minetest-windos.md)  | Windows focused minetest flavours and downloads      |
| [Minetest on MAC](minetest-macos.md)    | Mac support as most possible, including newer versions M1/M2 |
| [Androit options](minetest-androit.md)  | The many ways (mostly based on multicraft) to play minetest on androit phones |
| [IOs phones](minetest-ios.md)           | The many ways (mostly based on multicraft) to play minetest on IOS phones |
| [Cheaters clients](minetest-cheaters.md) | The modified clients to cheat and hack servers and the banned server list |


| Wiki game player                        | Observations                                         |
| --------------------------------------- | ---------------------------------------------------- |
| [Mods list](mods.md)                    | These are like plugins that you can add to enchance each game |
| [Games lists](games.md)                 | These represent the templates to generate the worlds you will play |
| [Worlds list](worlds.md)                | Wolrds are generally a dinamic resource made by the player or server, but can be pre generated |

> TODO: use badges here for best results rather a black bar

Developers' Center
------------------

[Devel Center](Devel_Center "wikilink")

The Developers' Center is largely for Final Minetest core developers and website admins. However, all visitors are welcome to read content.

