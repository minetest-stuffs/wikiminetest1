---
title: play-browser
description: play in web browser
published: 1
date: 2024-03-02T13:48:11.526Z
tags: minetest, play, engine
editor: markdown
dateCreated: 2024-03-02T13:42:20.372Z
---

## Introduction

This page lets you try out Minetest in your web browser. No installation is needed. **Note:** Sorry, this might not work well on phones. It's mostly for people who have keyboards.

This feature is presently based on a git repo originally from Paradust7. A snapshot is hosted at, and may be downloaded using, the following link:

[https://git.minetest.io/paradust7/minetest-wasm](https://git.minetest.io/paradust7/minetest-wasm)

## Problems with Browser Play 

**There are many problems with the Browser Play feature. Read the list of problems below:**

-   This feature might be temporary.
-   Again, this might not work well with phones. It assumes that you have a keyboard. You can try it anyway.
-   Only some web browsers will work. Chrome and Edge should work as long as hardware acceleration is turned on. There is more about that below.
-   Your web browser must have something called hardware acceleration or WebGL turned on. It might already be that way. If Browser Play doesn't work, look for hardware acceleration in your browser settings and turn that setting on.
-   The Browser Play screen supports High, Medium, and Low resolutions. High resolution might be too slow. Low resolution might not work well. Try setting resolution to Medium initially after startup.
-   This feature uses a Minetest proxy run by Paradust of Dustlabs dot IO. The proxy will probably go away. If the proxy does go away, you won't be able to use Browser Play to join online worlds. However, you should still be able to create a small world and try it out.
-   We also can't take responsibility for security issues with the proxy.
-   There is no way to save worlds that you create in Browser Play. They disappear on exit.
-   If you use uBlock Origin or similar security filters, you may, or may not, need to tell them to whitelist the webpages that are linked further down.
-   Browser Play isn't very fast.

## How to use Browser Play 

If you'd still like to try this feature, proceed as follows:

1\. Optional: If you like, set up your web browser to use a browser proxy or a VPN. This isn't essential.

2\. Go to the following link:

[https://minetest.org/wasm/](https://minetest.org/wasm/)

If you'd like to use the arrow keys to move instead of the letter keys, use the following link instead:

[https://minetest.org/awasm/](https://minetest.org/awasm/)

Note: Only the actual arrow keys will work at the 2nd link. The number-pad arrow keys won't work.

3\. Some messages should appear.

If error messages about a JavaScript exception or a TypeError occur, you can disregard those messages as long as each only happens once.

After a minute, a Launch Minetest button should appear. If it doesn't appear, give up and close the window.

4\. If the Launch Minetest button appears, press it and wait a minute. A Minetest GUI should appear. If the GUI appears, click on the Join Game tab.

5\. A server list might appear. Whether or not one does, you should be able to visit Final Minetest worlds by entering hostname, port, and user information on the right side. Trolltest worlds might work as well. MultiCraft worlds probably won't work.

## Browser Play Screenshot 

This is a screenshot of a visit to Woofworld in Chrome:

![](/assets/mtwasm-woofworld.jpg)
