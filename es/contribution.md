---
title: contribucion
description: Como contribuir!? Como ayudar a contribuir a esta wiki!
published: 1
date: 2024-03-01T21:16:24.456Z
tags: espanol, help, ayuda
editor: markdown
dateCreated: 2024-03-01T19:04:14.495Z
---


Minetest Wiki es un sitio web destinado a almacenar información sobre el juego y cómo jugar, como bloques, elementos, entidades, etc.


¿Nuevo en la edición de wiki?
--------------------

¡¡No tengas miedo de editarlo!! hazlo tú mismo **¡es fácil! ¡A diferencia que la wiki de Trolltest, usamos una completa interfaz grafica! Simplemente haga clic en "Editar"**
pestaña en las páginas para editar su contenido. **¿No tienes una cuenta? no te preocupes, lee las siguientes pautas:**


¿Como empiezo?
---------------

Puede crear una cuenta o iniciar sesión antes de poder editar páginas sin que su dirección IP se muestre en el historial de edición.
¡Simplemente inicie sesión en http://git.minetest.org y espere la aprobación! ¡Se le enviará por correo la misma cuenta y una contraseña temporal!

Después de hacer clic en el botón Editar, tendrá un panel doble con vista previa en el lado derecho (uno único en teléfonos con vista previa en la segunda pestaña).

Luego, cuando termines de editar, ¡guárdalo y mira la nueva contribución que hiciste!

Politica y directrices
---------------------

La wiki puede ser editada por cualquier persona y se rige por el consenso de sus usuarios,
derivado de la colaboración civil entre ellos, sustentada en argumentos, pero:

1. Cualquier cosa que vaya en contra de la [LICENCIA de Minetest y/o pautas de uso](licenseusage.md) no está permitida en la wiki, pase lo que pase.
2. No destroce, envíe spam ni edite de ningún otro modo de forma disruptiva.
3. Mantenga su comportamiento civilizado y asuma buena fe. No acoses, aceches ni insultes a nadie. Aborda la acción, no la persona detrás de ella.
4. Evite engañar a los lectores y otros editores. Asegúrese de que sus contribuciones a los artículos sean verificables. No agregue especulaciones, contenido de parodia o engaños a los artículos.
5. No anuncie ninguna comunidad de fans ni servidores. Los artículos no deben contener información ni enlaces a comunidades de fans, excepto en las referencias citadas.
6. No copie contenido de otras fuentes, incluidas aquellas que tratan sobre Minetest.org. Si cree que existe una necesidad legítima de utilizar algún material protegido por derechos de autor como archivo de uso legítimo, márquelo como tal.
     * Si dicho material está alojado en git.minetest.io o en nuestros repositorios, se puede utilizar dentro de la wiki y consultarlo.
7. No enlace ni incruste videos externos en artículos, a menos que no haya otra forma de demostrar el fenómeno o que el video sea oficial. ¡No promocione sitios corporativos, por favor!
8. La wiki no es un servicio de alojamiento de archivos. Si un archivo cargado no se integra en una página útil poco después de la carga, puede eliminarse sin previo aviso. Se permite cargar imágenes para su espacio de usuario con la misma política.
9. Las cuentas con nombres de usuario ofensivos, engañosos o inapropiados pueden bloquearse indefinidamente.
     * El abuso de múltiples cuentas para evadir bloqueos u otra elusión de la política puede resultar en el bloqueo indefinido de estas cuentas.
     * Un editor puede tener más de una cuenta sólo si todas las cuentas utilizadas por el editor están claramente marcadas.
     * Por el contrario, cada cuenta sólo debe ser utilizada por una persona. Si un grupo u organización desea editar en la wiki, cada miembro individual que vaya a editar debe crear su propia cuenta.

Derechos de autor y contenido
---------------------

Todos los usuarios pueden reclamar derechos de autor si su primer contenido es propio pero no del contenido general,
Por ejemplo, si una página wiki tiene 30 ediciones confirmadas y el usuario solo coloca la primera y la última,
dicho usuario sólo puede reclamar y utilizar la primera edición y el contenido relacionado, la página debe ser
estancado y el contenido debe estar referido al tema respectivo, luego el resto de colaboradores deben
cree una nueva página, sustituya la relacionada y en la nueva página vincule la primera (movida).
La nueva página creada no debe tener la misma redacción y secciones si el usuario anterior reclama
derechos de autor del contenido antiguo y es la estructura principal de la misma.


[![Minetest.org](https://img.shields.io/badge/WebPage-minetest-blue)](http://minetest.org)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-orange.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

