---
title: minetest inicio
description: Pagina principal de la wiki minetest pero en espanol
published: 1
date: 2024-03-01T21:16:24.456Z
tags: home, index, latino, espanol, minetest
editor: markdown
dateCreated: 2024-03-01T19:04:14.495Z
---

<strong>Bienvenido a la wiki de Minetest.</strong> Si desea editar aquí, consulte [página de cómo contribuir](contribute.md)

Minetest es un [motor de juego](https://en.wikipedia.org/wiki/Game_engine) gratuito denominado [voxel.](https://en.wikipedia.org/wiki/Voxel)

1. Piense en Minetest como un **Administrador de colecciones de programas. Cada programa puede crear o visitar mundos de juegos en bloques**
2. Los **mundos de juego en bloques tienen temas como el Viejo Oeste, el posapocalipsis, ciudades comunes y muchas más**.
3. **¡La jugabilidad varía dependiendo de las reglas de esos mundos de juego**! ¡Se puede descargar o reproducir en línea!

![imagenes.jpeg](/assets/images.jpeg){width="400"}

¡Callese! ¿¡Dime Cómo jugar!?
=======================

1. Descargue el programa [muchas opciones de motores de administrador] (minetest-downloads.md) y ejecútelo.
2. ¡Explore los numerosos [mundos de juego] (minetest-games.md) y ejecútelo más tarde!
3. ¡O navega por la [red de servidores minetest] (minetest-servers.md) para jugar con amigos!

-------------------------------------------------- ----------------------

Lo básico es que **Final Minetest** incluye programas de servidor (que ejecuta el mundo del juego) y cliente (que se conecta al servidor para jugar los mundos) de diferentes tipos. Su programa de servidor principal, **finetestserver**, funciona con todos los principales clientes de Minetest con fecha hasta mediados de 2023.

> Nota: Los clientes Trolltest y MultiCraft posteriores a 2023 solo son compatibles con las versiones más recientes

Este sitio web se centra en Final Minetest pero incluye cierta documentación de Trolltest ya que es parcialmente compatible.

Paginas para usuarios
---------------

| Enlace wiki | Observaciones |
| --------------------------------------- | -------------------------------------------------- -- |
| [Jugar en el navegador web](play-browser.md) | Permite con ciertas limitantes probar el juego pero es muy lento y limitado |
| [Reproducir en Linux](minetest-packages.md) | Cuidamos tu linux, marca esto si quieres un paquete para instalación |
| [Prueba de minas de Windows](minetest-windos.md) | Descargas y sabores de minetest centrados en Windows |
| [Minetest en MAC](minetest-macos.md) | La mayor compatibilidad posible con Mac, incluidas las versiones más recientes M1/M2 |
| [Opciones de Android](minetest-androit.md) | Las muchas formas (principalmente basadas en multicraft) de jugar minetest en teléfonos Android |
| [Teléfonos IOs](minetest-ios.md) | Las muchas formas (principalmente basadas en multicraft) de jugar minetest en teléfonos IOS |
| [Clientes tramposos](minetest-cheaters.md) | Los clientes modificados para engañar y hackear servidores y la lista de servidores prohibidos |


| Recurso para el minetest | Observaciones |
| --------------------------------------- | -------------------------------------------------- -- |
| [Lista de modificaciones](mods.md) | Estos son como complementos que puedes agregar para mejorar cada juego |
| [Listas de juegos](games.md) | Estas representan las plantillas para generar los mundos que jugarás |
| [Lista de mundos](worlds.md) | Los mundos son generalmente un recurso dinámico creado por el jugador o el servidor, pero pueden generarse previamente |

> TODO: use insignias aquí para obtener mejores resultados en lugar de una barra negra

Centro de desarrolladores
------------------

[Centro de desarrollo](Devel_Center "wikilink")

El Centro de desarrolladores es principalmente para desarrolladores principales y administradores de sitios web de Final Minetest. Sin embargo, todos los visitantes pueden leer el contenido.