---
title: Jugar en el navegador
description: Jugar el minetest desde el navegador web
published: 1
date: 2024-03-02T14:48:34.000Z
tags: minetest, play, espanol, jugar
editor: markdown
dateCreated: 2024-03-02T14:48:31.521Z
---

## Introducción

Esta página le permite probar Minetest en su navegador web. No se necesita instalación. Nota: Lo sentimos, es posible que esto no funcione bien en teléfonos. Es principalmente para personas que tienen teclados.

Esta característica se basa actualmente en un repositorio de git originario de Paradust7. Una instantánea está alojada y se puede descargar mediante el siguiente enlace:

[https://git.minetest.io/paradust7/minetest-wasm](https://git.minetest.io/paradust7/minetest-wasm)


### Problemas con la reproducción del navegador 

Hay muchos problemas con la función Browser Play. Lea la lista de problemas a continuación:

* Esta característica puede ser temporal. Nuevamente, es posible que esto no funcione bien con los teléfonos. * Se supone que tienes un teclado. Puedes intentarlo de todos modos.
* Sólo funcionarán algunos navegadores web. Chrome y Edge deberían funcionar siempre que la aceleración de hardware esté activada. Hay más sobre eso a continuación.
* Su navegador web debe tener activado algo llamado aceleración de hardware o WebGL. Puede que ya sea así. Si Browser Play no funciona, busque la aceleración de hardware en la configuración de su navegador y active esa configuración.
* La pantalla Browser Play admite resoluciones alta, media y baja. La alta resolución puede ser demasiado lenta. Es posible que la resolución baja no funcione bien. Intente configurar la resolución en Media inicialmente después del inicio.
* Esta función utiliza un proxy Minetest ejecutado por Paradust de Dustlabs dot IO. El proxy probablemente desaparecerá. Si el proxy desaparece, no podrás usar Browser Play para unirte a mundos en línea. Sin embargo, aún deberías poder crear un mundo pequeño y probarlo.
* Tampoco podemos asumir responsabilidad por problemas de seguridad con el proxy.
* No hay forma de guardar los mundos que creas en Browser Play. Desaparecen al salir.
* Si utiliza uBlock Origin o filtros de seguridad similares, es posible que deba indicarles o no que incluyan en la lista blanca las páginas web que están vinculadas más abajo.
* Browser Play no es muy rápido.

### Cómo utilizar la reproducción con navegador 

Si aún desea probar esta función, proceda de la siguiente manera:

1. Opcional: si lo desea, configure su navegador web para usar un proxy de navegador o una VPN. Esto no es esencial.

2. Vaya al siguiente enlace:

https://minetest.org/wasm/

Si desea utilizar las teclas de flecha para moverse en lugar de las teclas de letras, utilice el siguiente enlace:

https://minetest.org/awasm/

Nota: Sólo las teclas de flecha reales funcionarán en el segundo enlace. Las teclas de flecha del teclado numérico no funcionan.

3. Deberían aparecer algunos mensajes.

Si se producen mensajes de error sobre una excepción de JavaScript o un TypeError, puede ignorar esos mensajes siempre que cada uno de ellos solo ocurra una vez.

Después de un minuto, debería aparecer un botón Iniciar Minetest. Si no aparece, desiste y cierra la ventana.

4. Si aparece el botón Iniciar Minetest, presiónelo y espere un minuto. Debería aparecer una GUI de Minetest. Si aparece la GUI, haga clic en la pestaña Unirse al juego.

5. Es posible que aparezca una lista de servidores. Ya sea que lo haga o no, debería poder visitar los mundos de Final Minetest ingresando el nombre de host, el puerto y la información del usuario en el lado derecho. Los mundos trolltest también podrían funcionar. Los mundos MultiCraft probablemente no funcionarán.


### Captura de pantalla de reproducción del navegador 

Esta es una captura de pantalla de una visita a Woofworld en Chrome:

![mtwasm-woofworld.jpg](/assets/mtwasm-woofworld.jpg)